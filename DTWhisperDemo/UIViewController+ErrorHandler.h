//
//  UIViewController+ErrorHandler.h
//  DTWhisperDemo
//
//  Created by EdenLi on 2018/8/2.
//  Copyright © 2018年 Darktt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ErrorHandler)

/**
 *  Present error with UIAlertController, alert title will use localizeable "Error" string
 *  and with a dismiss button(localizeable too).
 *
 *  @param error The error object will present.
 */
- (void)presentErrorAlertWithError:(NSError *)error;

/**
 *  Present alert with UIAlertController, this alert used single button,
 *  that button title will use localizeable "Dismiss" string.
 *
 *  @param title   The alert title to present.
 *  @param message The alert message to present.
 */
- (void)presentAlertWithTitle:(nullable NSString *)title message:(NSString *)message;

/**
 *    Present alert with UIAlertController.
 *
 *    @param title       Custom title.
 *    @param message     Custom message.
 *    @param alertHandler Handle after cancel button pressed to execut block task.
 */
- (void)presentAlertWithTitle:(nullable NSString *)title message:(NSString *)message handler:(nullable void (^) (void))alertHandler;

/**
 *    Present alert with UIAlertController.
 *
 *    @param title       Custom title.
 *    @param message     Custom message.
 *    @param cancelTitle Custom cancel title for button.
 */
- (void)presentAlertWithTitle:(nullable NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelTitle;

/**
 *    Present alert with UIAlertController.
 *
 *    @param title       Custom title.
 *    @param message     Custom message.
 *    @param cancelTitle Custom cancel title for button.
 *    @param alertHandler Handle after cancel button pressed to execut block task.
 */
- (void)presentAlertWithTitle:(nullable NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelTitle handler:(nullable void (^) (void))alertHandler;

@end
