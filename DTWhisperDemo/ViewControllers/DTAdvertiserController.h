//
//  DTAdvertiserController.h
//  DTWhisperDemo
//
//  Created by EdenLi on 2018/8/2.
//  Copyright © 2018年 Darktt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DTAdvertiserController : UIViewController

+ (instancetype)advertiserController;

@end
