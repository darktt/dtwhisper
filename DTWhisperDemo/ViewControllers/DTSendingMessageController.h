//
//  DTSendingMessageController.h
//  DTWhisperDemo
//
//  Created by EdenLi on 2018/8/3.
//  Copyright © 2018年 Darktt. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MCPeerID, MCSession;
@interface DTSendingMessageController : UIViewController

@property (weak, nonatomic) NSMutableArray<MCPeerID *> *peers;

+ (instancetype)sendingMessageControllerWithSession:(MCSession *)session;

@end
