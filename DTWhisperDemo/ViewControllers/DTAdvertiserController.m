//
//  DTAdvertiserController.m
//  DTWhisperDemo
//
//  Created by EdenLi on 2018/8/2.
//  Copyright © 2018年 Darktt. All rights reserved.
//

@import MultipeerConnectivity;

#import "DTAdvertiserController.h"
#import "UIViewController+ErrorHandler.h"

#import "DTPeerManager.h"

static NSString *kServiceType = @"mr-pos";

@interface DTAdvertiserController () <MCNearbyServiceAdvertiserDelegate, MCSessionDelegate>

@property (strong, nonatomic) MCNearbyServiceAdvertiser *advertiser;
@property (strong, nonatomic) MCSession *session;
@property (strong, nonatomic) NSMutableArray<MCPeerID *> *peers;

@property (weak, nonatomic) IBOutlet UILabel *receiveLabel;
@property (weak, nonatomic) IBOutlet UITextField *inputField;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@end

@implementation DTAdvertiserController

+ (instancetype)advertiserController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DTAdvertiserController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"DTAdvertiserController"];
    
    return viewController;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    NSMutableArray *peers = [NSMutableArray arrayWithCapacity:0];
    
    [self setPeers:peers];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    DTPeerManager *manager = [DTPeerManager sharedManager];
    NSArray *peerIDs = [manager restorePeers];
    
    if (peerIDs.count != 0) {
        
        [self.peers addObjectsFromArray:peerIDs];
        [self connectPeers];
        return;
    }
    [self advertising];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.advertiser stopAdvertisingPeer];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"Advertiser"];
    
    [self.receiveLabel setText:@"[未連線]"];
    [self.sendButton addTarget:self action:@selector(sendMessageAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action -

- (void)sendMessageAction:(id)sender
{
    NSString *message = self.inputField.text;
    
    if ([message isEqualToString:@""]) {
        return;
    }
    
    [self sendingDataWithMessage:message];
}

#pragma mark - Private Method -

- (void)connectPeers
{
    UIDevice *device = [UIDevice currentDevice];
    NSString *name = device.name;
    MCPeerID *peerID = [[MCPeerID alloc] initWithDisplayName:name];
    MCPeerID *clientPeerID = self.peers.firstObject;
    
    void (^completionHandler) (NSData *connectionData, NSError *error) = ^(NSData *connectionData, NSError *error) {
        
        NSString *connectionString = [[NSString alloc] initWithData:connectionData encoding:NSUTF8StringEncoding];
        
        NSLog(@"Connection String: %@", connectionString);
    };
    
    MCSession *session = [[MCSession alloc] initWithPeer:peerID securityIdentity:nil encryptionPreference:MCEncryptionNone];
    [session nearbyConnectionDataForPeer:clientPeerID withCompletionHandler:completionHandler];
    [session connectPeer:<#(nonnull MCPeerID *)#> withNearbyConnectionData:<#(nonnull NSData *)#>]
    [session setDelegate:self];
    
    [self setSession:session];
}

- (void)advertising
{
    UIDevice *device = [UIDevice currentDevice];
    NSString *name = device.name;
    
    MCPeerID *peerID = [[MCPeerID alloc] initWithDisplayName:name];
    
    MCNearbyServiceAdvertiser *advertiser = [[MCNearbyServiceAdvertiser alloc] initWithPeer:peerID discoveryInfo:@{} serviceType:kServiceType];
    [advertiser setDelegate:self];
    [advertiser startAdvertisingPeer];
    
    [self setAdvertiser:advertiser];
    
    MCSession *session = [[MCSession alloc] initWithPeer:peerID securityIdentity:nil encryptionPreference:MCEncryptionNone];
    [session setDelegate:self];
    
    [self setSession:session];
}

- (void)invitationWithPeer:(MCPeerID *)peer result:(void (^) (BOOL))resultHandler
{
    NSString *acceptTitle = @"接受";
    NSString *denyTitle = @"拒絕";
    
    void (^actionHandler) (UIAlertAction *) = ^(UIAlertAction *action) {
        
        BOOL accepted = [action.title isEqualToString:acceptTitle];
        
        resultHandler(accepted);
        
        if (accepted) {
            DTPeerManager *manager = [DTPeerManager sharedManager];
            [manager addPeerWithPeerID:peer];
            [manager synchronize];
        }
    };
    UIAlertAction *acceptAction = [UIAlertAction actionWithTitle:acceptTitle style:UIAlertActionStyleDefault handler:actionHandler];
    UIAlertAction *denyAction = [UIAlertAction actionWithTitle:denyTitle style:UIAlertActionStyleCancel handler:actionHandler];
    
    NSString *message = [NSString stringWithFormat:@"[%@] 請求連線，是否接受？", peer.displayName];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"要求連線" message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:acceptAction];
    [alertController addAction:denyAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)peerIsConnectedWithPeer:(MCPeerID *)peer
{
    NSString *string = [NSString stringWithFormat:@"[%@] 已連線", peer.displayName];
    
    [self.receiveLabel setText:string];
}

- (void)peerIsConnectingWithPeer:(MCPeerID *)peer
{
    NSString *string = [NSString stringWithFormat:@"[%@] 連線中...", peer.displayName];
    
    [self.receiveLabel setText:string];
}

- (void)peerIsNotConnectedWithPeer:(MCPeerID *)peer
{
    NSString *string = [NSString stringWithFormat:@"[%@] 未連線", peer.displayName];
    
    [self.receiveLabel setText:string];
}

- (void)sendingDataWithMessage:(NSString *)message
{
    NSData *sendingData = [message dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *error = NULL;
    [self.session sendData:sendingData toPeers:self.peers withMode:MCSessionSendDataReliable error:&error];
    
    if (error != nil) {
        
        [self presentErrorAlertWithError:error];
    }
}

#pragma mark - Delegate Methods -
//MARK: MCNearbyServiceAdvertiserDelegate

- (void)advertiser:(MCNearbyServiceAdvertiser *)advertiser didReceiveInvitationFromPeer:(MCPeerID *)peerID withContext:(NSData *)context invitationHandler:(void (^)(BOOL, MCSession * _Nullable))invitationHandler
{
    void (^resultHandler) (BOOL) = ^(BOOL accepted) {
        
        MCSession *session = accepted ? self.session : nil;
        
        invitationHandler(accepted, session);
    };
    
    [self invitationWithPeer:peerID result:resultHandler];
}

- (void)advertiser:(MCNearbyServiceAdvertiser *)advertiser didNotStartAdvertisingPeer:(NSError *)error
{
    [self presentErrorAlertWithError:error];
}

//MARK: MCSessionDelegate

- (void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state
{
    void (^mainQueueBlock) (void) = ^{
      
        NSString *name = peerID.displayName;
        
        if (state == MCSessionStateNotConnected) {
            
            NSLog(@"[%@] not connected.", name);
            
            [self.peers removeObject:peerID];
            [self peerIsNotConnectedWithPeer:peerID];
        }
        
        if (state == MCSessionStateConnecting) {
            
            NSLog(@"[%@] connecting.", name);
            
            [self peerIsConnectingWithPeer:peerID];
        }
        
        if (state == MCSessionStateConnected) {
            
            NSLog(@"[%@] connected.", name);
            
            [self.peers addObject:peerID];
            [self peerIsConnectedWithPeer:peerID];
        }
    };
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:mainQueueBlock];
}

- (void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID
{
    void (^mainQueueBlock) (void) = ^{
        
        NSString *receiveString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        [self.receiveLabel setText:receiveString];
    };
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:mainQueueBlock];
}

- (void)session:(MCSession *)session didReceiveStream:(NSInputStream *)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID
{
    
}

- (void)session:(MCSession *)session didStartReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID withProgress:(NSProgress *)progress
{
    
}

- (void)session:(MCSession *)session didFinishReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID atURL:(NSURL *)localURL withError:(NSError *)error
{
    
}

@end
