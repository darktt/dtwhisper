//
//  ViewController.m
//  DTWhisperDemo
//
//  Created by EdenLi on 2018/8/2.
//  Copyright © 2018年 Darktt. All rights reserved.
//

@import MultipeerConnectivity;

#import "ViewController.h"
#import "DTAdvertiserController.h"
#import "DTDiscoveringController.h"

@interface ViewController ()

- (IBAction)advertiserAction:(id)sender;
- (IBAction)discoveringAction:(id)sender;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self setTitle:@"Main"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions -

- (void)advertiserAction:(id)sender
{
    DTAdvertiserController *viewController = [DTAdvertiserController advertiserController];
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)discoveringAction:(id)sender
{
    DTDiscoveringController *viewController = [DTDiscoveringController discoveringController];
    
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
