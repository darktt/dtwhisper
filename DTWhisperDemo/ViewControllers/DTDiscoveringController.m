//
//  DTDiscoveringController.m
//  DTWhisperDemo
//
//  Created by EdenLi on 2018/8/2.
//  Copyright © 2018年 Darktt. All rights reserved.
//

@import MultipeerConnectivity;

#import "DTDiscoveringController.h"

#import "UIViewController+ErrorHandler.h"
#import "DTSendingMessageController.h"

static NSString *kServiceType = @"mr-pos";

@interface DTDiscoveringController () <UITableViewDataSource, UITableViewDelegate, MCNearbyServiceBrowserDelegate, MCSessionDelegate>

@property (strong, nonatomic) MCNearbyServiceBrowser *browser;
@property (strong, nonatomic) MCSession *session;
@property (strong, nonatomic) NSMutableArray<MCPeerID *> *peers;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation DTDiscoveringController

+ (instancetype)discoveringController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DTDiscoveringController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"DTDiscoveringController"];
    
    return viewController;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    NSMutableArray *peers = [NSMutableArray arrayWithCapacity:0];
    
    [self setPeers:peers];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.session setDelegate:self];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.browser stopBrowsingForPeers];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"Discovering"];
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    
    [self discovering];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Method

- (void)discovering
{
    UIDevice *device = [UIDevice currentDevice];
    NSString *name = device.name;
    
    MCPeerID *peerID = [[MCPeerID alloc] initWithDisplayName:name];
    
    MCNearbyServiceBrowser *browser = [[MCNearbyServiceBrowser alloc] initWithPeer:peerID serviceType:kServiceType];
    [browser setDelegate:self];
    [browser startBrowsingForPeers];
    
    [self setBrowser:browser];
    
    MCSession *session = [[MCSession alloc] initWithPeer:peerID securityIdentity:nil encryptionPreference:MCEncryptionNone];
    
    [self setSession:session];
}

- (void)pushSendingMessageController
{
    DTSendingMessageController *viewController = [DTSendingMessageController sendingMessageControllerWithSession:self.session];
    [viewController setPeers:self.peers];
    
    [self.session setDelegate:nil];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - Delegate Methods -
#pragma mark UITableView DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.peers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    
    MCPeerID *peer = self.peers[indexPath.row];
    
    [cell.textLabel setText:peer.displayName];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
}

#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    MCPeerID *peer = self.peers[indexPath.row];
    [self.browser invitePeer:peer toSession:self.session withContext:nil timeout:120.0f];
}

//MARK: MCNearbyServiceBrowserDelegate

- (void)browser:(MCNearbyServiceBrowser *)browser foundPeer:(MCPeerID *)peerID withDiscoveryInfo:(NSDictionary<NSString *,NSString *> *)info
{
    [self.peers addObject:peerID];
    
    NSIndexSet *sections = [NSIndexSet indexSetWithIndex:0];
    [self.tableView reloadSections:sections withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)browser:(MCNearbyServiceBrowser *)browser lostPeer:(MCPeerID *)peerID
{
    [self.peers removeObject:peerID];
    
    NSIndexSet *sections = [NSIndexSet indexSetWithIndex:0];
    [self.tableView reloadSections:sections withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)browser:(MCNearbyServiceBrowser *)browser didNotStartBrowsingForPeers:(NSError *)error
{
    [self presentErrorAlertWithError:error];
}

//MARK: MCSessionDelegate

- (void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state
{
    void (^mainQueueBlock) (void) = ^{
        NSString *name = peerID.displayName;
        
        if (state == MCSessionStateNotConnected) {
            
            NSLog(@"[%@] not connected.", name);
            [self.peers removeObject:peerID];
        }
        
        if (state == MCSessionStateConnecting) {
            
            NSLog(@"[%@] connecting.", name);
        }
        
        if (state == MCSessionStateConnected) {
            
            NSLog(@"[%@] connected.", name);
            [self.peers addObject:peerID];
            [self pushSendingMessageController];
        }
    };
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:mainQueueBlock];
}

- (void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID
{
    NSString *receiveString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSLog(@"[%@] send message: %@", peerID.displayName, receiveString);
}

- (void)session:(MCSession *)session didReceiveStream:(NSInputStream *)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID
{
    
}

- (void)session:(MCSession *)session didStartReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID withProgress:(NSProgress *)progress
{
    
}

- (void)session:(MCSession *)session didFinishReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID atURL:(NSURL *)localURL withError:(NSError *)error
{
    
}

@end
