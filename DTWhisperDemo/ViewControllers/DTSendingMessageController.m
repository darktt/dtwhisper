//
//  DTSendingMessageController.m
//  DTWhisperDemo
//
//  Created by EdenLi on 2018/8/3.
//  Copyright © 2018年 Darktt. All rights reserved.
//

@import MultipeerConnectivity;

#import "DTSendingMessageController.h"
#import "UIViewController+ErrorHandler.h"

@interface DTSendingMessageController () <MCSessionDelegate>

@property (weak, nonatomic) MCSession *session;

@property (weak, nonatomic) IBOutlet UILabel *receiveLabel;
@property (weak, nonatomic) IBOutlet UITextField *inputField;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@end

@implementation DTSendingMessageController

+ (instancetype)sendingMessageControllerWithSession:(MCSession *)session
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DTSendingMessageController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"DTSendingMessageController"];
    [viewController setSession:session];
    
    return viewController;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.session setDelegate:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"Sending Message"];
    
    [self peerIsConnectedWithPeer:self.peers.firstObject];
    [self.sendButton addTarget:self action:@selector(sendMessageAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action -

- (void)sendMessageAction:(id)sender
{
    NSString *message = self.inputField.text;
    
    if ([message isEqualToString:@""]) {
        return;
    }
    
    [self sendingDataWithMessage:message];
}

#pragma mark - Private Method -

- (void)peerIsConnectedWithPeer:(MCPeerID *)peer
{
    NSString *string = [NSString stringWithFormat:@"[%@] 已連線", peer.displayName];
    
    [self.receiveLabel setText:string];
}

- (void)peerIsConnectingWithPeer:(MCPeerID *)peer
{
    NSString *string = [NSString stringWithFormat:@"[%@] 連線中...", peer.displayName];
    
    [self.receiveLabel setText:string];
}

- (void)peerIsNotConnectedWithPeer:(MCPeerID *)peer
{
    NSString *string = [NSString stringWithFormat:@"[%@] 未連線", peer.displayName];
    
    [self.receiveLabel setText:string];
}

- (void)sendingDataWithMessage:(NSString *)message
{
    NSData *sendingData = [message dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *error = NULL;
    [self.session sendData:sendingData toPeers:self.peers withMode:MCSessionSendDataReliable error:&error];
    
    if (error != nil) {
        
        [self presentErrorAlertWithError:error];
    }
}

#pragma mark - Delegate Methods -
//MARK: MCSessionDelegate

- (void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state
{
    void (^mainQueueBlock) (void) = ^{
        
        NSString *name = peerID.displayName;
        
        if (state == MCSessionStateNotConnected) {
            
            NSLog(@"[%@] not connected.", name);
            
            [self.peers removeObject:peerID];
            [self peerIsNotConnectedWithPeer:peerID];
        }
        
        if (state == MCSessionStateConnecting) {
            
            NSLog(@"[%@] connecting.", name);
            
            [self peerIsConnectingWithPeer:peerID];
        }
        
        if (state == MCSessionStateConnected) {
            
            NSLog(@"[%@] connected.", name);
            
            [self.peers addObject:peerID];
            [self peerIsConnectedWithPeer:peerID];
        }
    };
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:mainQueueBlock];
}

- (void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID
{
    void (^mainQueueBlock) (void) = ^{
        
        NSString *receiveString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        [self.receiveLabel setText:receiveString];
    };
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:mainQueueBlock];
}

- (void)session:(MCSession *)session didReceiveStream:(NSInputStream *)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID
{
    
}

- (void)session:(MCSession *)session didStartReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID withProgress:(NSProgress *)progress
{
    
}

- (void)session:(MCSession *)session didFinishReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID atURL:(NSURL *)localURL withError:(NSError *)error
{
    
}

@end
