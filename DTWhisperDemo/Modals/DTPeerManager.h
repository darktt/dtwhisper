//
//  DTPeerManager.h
//  DTWhisperDemo
//
//  Created by EdenLi on 2018/8/14.
//  Copyright © 2018年 Darktt. All rights reserved.
//

@import Foundation;

NS_ASSUME_NONNULL_BEGIN
@class MCPeerID;
@interface DTPeerManager : NSObject

+ (instancetype)sharedManager;

- (void)addPeerWithPeerID:(MCPeerID *)peerID;
- (void)setPeersWithArray:(nullable NSArray<MCPeerID *> *)peerIDs;

- (nullable NSArray<MCPeerID *> *)restorePeers;

- (void)synchronize;

@end
NS_ASSUME_NONNULL_END
