//
//  DTPeerManager.m
//  DTWhisperDemo
//
//  Created by EdenLi on 2018/8/14.
//  Copyright © 2018年 Darktt. All rights reserved.
//

@import MultipeerConnectivity.MCPeerID;

#import "DTPeerManager.h"

static DTPeerManager *singletion = nil;
static NSString * const kUserDefaultsPeerKey = @"UserDefaultsPeerKey";

@interface DTPeerManager ()

@property (assign, nonatomic) NSUserDefaults *userDefaults;
@property (retain, nonatomic) NSMutableArray<MCPeerID *> *peerIDs;

@end

@implementation DTPeerManager

+ (instancetype)sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        singletion = [DTPeerManager new];
    });
    
    return singletion;
}

- (instancetype)init
{
    self = [super init];
    if (self == nil) return nil;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *archivedData = [userDefaults dataForKey:kUserDefaultsPeerKey];;
    
    [self setUserDefaults:userDefaults];
    [self unarchiverWithData:archivedData];
    
    return self;
}

- (void)addPeerWithPeerID:(MCPeerID *)peerID
{
    [self.peerIDs addObject:peerID];
}

- (void)setPeersWithArray:(NSArray<MCPeerID *> *)peerIDs
{
    [self.peerIDs removeAllObjects];
    
    if (peerIDs != nil) {
        
        [self.peerIDs addObjectsFromArray:peerIDs];
    }
}

- (NSArray<MCPeerID *> *)restorePeers
{
    NSArray<MCPeerID *> *peerIDs = [self.peerIDs copy];
    
    return [peerIDs autorelease];
}

- (void)synchronize
{
    NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:self.peerIDs];
    
    [self.userDefaults setObject:archivedData forKey:kUserDefaultsPeerKey];
    [self.userDefaults synchronize];
}

#pragma mark - Private Method -

- (void)unarchiverWithData:(NSData *)data
{
    NSArray *unarchiverObject = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSMutableArray *peerIDs = nil;
    
    if (unarchiverObject != nil) {
        
        peerIDs = [NSMutableArray arrayWithArray:unarchiverObject];
    } else {
        
        peerIDs = [NSMutableArray arrayWithCapacity:0];
    }
    
    [self setPeerIDs:peerIDs];
}

@end
