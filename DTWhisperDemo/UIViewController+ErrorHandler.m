//
//  UIViewController+ErrorHandler.m
//  DTWhisperDemo
//
//  Created by EdenLi on 2018/8/2.
//  Copyright © 2018年 Darktt. All rights reserved.
//

#import "UIViewController+ErrorHandler.h"

@implementation UIViewController (ErrorHandler)

- (void)presentErrorAlertWithError:(NSError *)error
{
    NSString *title = NSLocalizedString(@"Error", @"");
    NSString *message = [NSString stringWithFormat:@"%@(%tu)", error.localizedDescription, error.code];
    
    [self presentAlertWithTitle:title message:NSLocalizedString(message, @"")];
}

- (void)presentAlertWithTitle:(NSString *)title message:(NSString *)message
{
    NSString *cancelTitle = NSLocalizedString(@"Dismiss", @"");
    [self presentAlertWithTitle:title message:message cancelButtonTitle:cancelTitle handler:nil];
}

- (void)presentAlertWithTitle:(NSString *)title message:(NSString *)message handler:(void (^)(void))alertHandler
{
    NSString *cancelTitle = NSLocalizedString(@"Dismiss", @"");
    [self presentAlertWithTitle:title message:message cancelButtonTitle:cancelTitle handler:alertHandler];
}

- (void)presentAlertWithTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelTitle
{
    [self presentAlertWithTitle:title message:message cancelButtonTitle:cancelTitle handler:nil];
}

- (void)presentAlertWithTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelTitle handler:(void (^)(void))alertHandler
{
    void (^handler) (UIAlertAction *) = ^(UIAlertAction *action) {
        
        if (alertHandler != nil) {
            alertHandler();
        }
    };
    
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:cancelTitle style:UIAlertActionStyleCancel handler:handler];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:alertAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
